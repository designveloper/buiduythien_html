1/ Introducing HTML:

  - The importance of HTML

	+ HTML provides the essential structure for web pages

	+ It's a markup language

  - Basic HTML syntax

	+ Content in HTML is identified by wrapping it in tag

	+ Create more complicated structures by nesting one element inside of another one.

  - HTML resources:

	+ https://www.w3.org/

	+ https://www.whatwg.org/

	+ https://www.webplatform.org/

	+ https://developer.mozilla.org/

2/ Basic Page Structure:

  - DOCTYPE declaration: https://www.w3.org/QA/2002/04/valid-dtd-list.html

  - Content Models: 

	+ Define the type of content expected inside an element and control syntax rules.

	+ In HTML5,there are seven main models: flow, metadata, embedded, interactive, heading, phrasing, sectioning. 

	(https://www.w3.org/TR/2011/WD-html5-20110525/content-models.html)

3/ Formatting Page Content:

  - Preformatted text basically tells the browser that I want this 

text to display.

  - Fomatting HTML content is wrap it in the appropriate tag

4/ Structuring Content:

  - Sectioning Elements: <h1> ... <h6>, <article>, <aside>, <nav>, <section>

  - Sematic Elements: <header>, <main>, <footer>

  -> Use those elements to group content into clearly defined regions, establish the relationship between those regions and make the page more readable for both people and machines.

5/ Creating Links:

6/Creating Lists:

7/ Controlling Styling and Basic Scripting:

  - HTML and CSS:
  
	+ Most browsers agree on default styling
	
	+ Cascading Style Sheets (CSS) is to overwrite the default style
	
	+ CSS is an equally critical part of learning web design
  
  - HTML and Javascript: 
  
	+ JS can access the basic structure and elements of a page and then control them through scripting